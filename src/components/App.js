import { Route, Switch } from 'react-router-dom';
import { AuthProvider } from '../contexts/AuthContext';

import Layout from './layout/Layout';
import PrivateRoute from './layout/PrivateRoute';

import HomePage from '../pages/Home';
import LoginPage from '../pages/Login';
import SubscribePage from '../pages/Subscribe';
import DashboardPage from '../pages/Dashboard';
import ForgotPasswordPage from '../pages/ForgotPassword.js';
import UpdateProfilePage from '../pages/UpdateProfile.js';

function App() {
	return (
		<AuthProvider>
			<Layout>
				<Switch>
					<Route exact path="/" component={HomePage} />
					<Route path="/connexion" component={LoginPage} />
					<Route path="/inscription" component={SubscribePage} />
					<Route path="/motdepasseoublie" component={ForgotPasswordPage} />
					<PrivateRoute path="/profil" component={DashboardPage} />
					<PrivateRoute path="/modifierprofil" component={UpdateProfilePage} />
				</Switch>
			</Layout>
		</AuthProvider>
	);
}

export default App;
