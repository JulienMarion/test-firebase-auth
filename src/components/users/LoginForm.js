import React, { useRef, useState } from 'react';
import { Card, Form, Button, Alert } from 'react-bootstrap';
import { useAuth } from '../../contexts/AuthContext';
import { Link, useHistory } from 'react-router-dom';

export default function Login() {
	const emailRef = useRef();
	const passwordRef = useRef();

	const { login } = useAuth();

	const [error, setError] = useState('');
	const [loading, setLoading] = useState(false);

	const history = useHistory();

	async function handleSumbit(event) {
		event.preventDefault();

		try {
			setError('');
			setLoading(true);
			await login(emailRef.current.value, passwordRef.current.value);
			history.push('/profil');
		} catch {
			setError('Echec de la connexion');
		}

		setLoading(false);
	}
	return (
		<>
			<Card>
				<Card.Body>
					<h2 className="text-center mb-4">Se connecter</h2>
					{error && <Alert variant="danger">{error}</Alert>}
					<Form onSubmit={handleSumbit}>
						<Form.Group id="email">
							<Form.Label>Email</Form.Label>
							<Form.Control type="email" ref={emailRef} required />
						</Form.Group>
						<Form.Group id="password">
							<Form.Label>Mot de passe</Form.Label>
							<Form.Control type="password" ref={passwordRef} required />
						</Form.Group>
						<Button disabled={loading} type="submit" className="w-100 mt-3">
							Se connecter
						</Button>
					</Form>
					<div className="w100 text-center mt-3">
						<Link to="motdepasseoublie">Mot de passe oublié?</Link>
					</div>
				</Card.Body>
			</Card>
			<div className="w100 text-center mt-2">
				Pas de compte? <Link to="/inscription">S'inscrire!</Link>
			</div>
		</>
	);
}
