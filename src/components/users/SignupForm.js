import React, { useRef, useState } from 'react';
import { Card, Form, Button, Alert } from 'react-bootstrap';
import { useAuth } from '../../contexts/AuthContext';
import { Link, useHistory } from 'react-router-dom';

export default function Signup() {
	const emailRef = useRef();
	const passwordRef = useRef();
	const passwordConfirmRef = useRef();

	const { signup } = useAuth();

	const [error, setError] = useState('');
	const [message, setMessage] = useState('');
	const [loading, setLoading] = useState(false);

	const history = useHistory();

	async function handleSumbit(event) {
		event.preventDefault();

		if (passwordRef.current.value !== passwordConfirmRef.current.value) {
			return setError('La confirmation ne correspond pas au mot de passe');
		}
		try {
			setMessage('');
			setError('');
			setLoading(true);
			await signup(emailRef.current.value, passwordRef.current.value);
			setMessage('Inscription réussie !');
			history.push('/connexion');
		} catch {
			setError('Echec de la création du compte');
		}

		setLoading(false);
	}
	return (
		<>
			<Card>
				<Card.Body>
					<h2 className="text-center mb-4">S'inscrire</h2>
					{error && <Alert variant="danger">{error}</Alert>}
					{message && <Alert variant="success">{message}</Alert>}
					<Form onSubmit={handleSumbit}>
						<Form.Group id="email">
							<Form.Label>Email</Form.Label>
							<Form.Control type="email" ref={emailRef} required />
						</Form.Group>
						<Form.Group id="password">
							<Form.Label>Mot de passe</Form.Label>
							<Form.Control type="password" ref={passwordRef} required />
						</Form.Group>
						<Form.Group id="passwordConfirm">
							<Form.Label>Confirmation du mot de passe</Form.Label>
							<Form.Control type="password" ref={passwordConfirmRef} required />
						</Form.Group>
						<Button disabled={loading} type="submit" className="w-100 mt-3">
							S'inscrire
						</Button>
					</Form>
				</Card.Body>
			</Card>
			<div className="w100 text-center mt-2">
				Déjà inscrit? <Link to="/connexion">Se connecter!</Link>
			</div>
		</>
	);
}
