import React, { useRef, useState } from 'react';
import { Card, Form, Button, Alert } from 'react-bootstrap';
import { useAuth } from '../../contexts/AuthContext';
import { Link } from 'react-router-dom';

export default function ForgotPassword() {
	const emailRef = useRef();

	const { resetPassword } = useAuth();

	const [error, setError] = useState('');
	const [message, setMessage] = useState('');
	const [loading, setLoading] = useState(false);

	async function handleSumbit(event) {
		event.preventDefault();

		try {
			setMessage('');
			setError('');
			setLoading(true);
			await resetPassword(emailRef.current.value);
			setMessage("Regardez votre boite mail pour plus d'informations");
		} catch {
			setError('Echec de la réinitialisation du mot de passe');
		}

		setLoading(false);
	}
	return (
		<>
			<Card>
				<Card.Body>
					<h2 className="text-center mb-4">Réinitialiser le mot de passe</h2>
					{error && <Alert variant="danger">{error}</Alert>}
					{message && <Alert variant="success">{message}</Alert>}
					<Form onSubmit={handleSumbit}>
						<Form.Group id="email">
							<Form.Label>Email</Form.Label>
							<Form.Control type="email" ref={emailRef} required />
						</Form.Group>
						<Button disabled={loading} type="submit" className="w-100 mt-3">
							Réinitialiser le mot de passe
						</Button>
					</Form>
					<div className="w100 text-center mt-3">
						<Link to="connexion">Se connecter</Link>
					</div>
				</Card.Body>
			</Card>
			<div className="w100 text-center mt-2">
				Pas de compte? <Link to="/inscription">S'inscrire!</Link>
			</div>
		</>
	);
}
