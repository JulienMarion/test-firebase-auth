import React, { useRef, useState } from 'react';
import { Card, Form, Button, Alert } from 'react-bootstrap';
import { useAuth } from '../../contexts/AuthContext';
import { Link, useHistory } from 'react-router-dom';

export default function UpdateProfileForm() {
	const emailRef = useRef();
	const passwordRef = useRef();
	const passwordConfirmRef = useRef();

	const { currentUser, updatePassword, updateEmail } = useAuth();

	const [error, setError] = useState('');
	const [loading, setLoading] = useState(false);

	const history = useHistory();

	function handleSumbit(event) {
		event.preventDefault();

		if (passwordRef.current.value !== passwordConfirmRef.current.value) {
			return setError('La confirmation ne correspond pas au mot de passe');
		}

		const promises = [];
		setLoading(true);
		setError('');

		if (emailRef.current.value !== currentUser.email) {
			promises.push(updateEmail(emailRef.current.value));
		}

		if (passwordRef.current.value) {
			promises.push(updatePassword(passwordRef.current.value));
		}

		Promise.all(promises)
			.then(() => {
				history.push('/');
			})
			.catch(() => {
				setError('Echec de la modification');
			})
			.finally(() => {
				setLoading(false);
			});
	}
	return (
		<>
			<Card>
				<Card.Body>
					<h2 className="text-center mb-4">Modifier le profil</h2>
					{error && <Alert variant="danger">{error}</Alert>}
					<Form onSubmit={handleSumbit}>
						<Form.Group id="email">
							<Form.Label>Email</Form.Label>
							<Form.Control type="email" ref={emailRef} required defaultValue={currentUser.email} />
						</Form.Group>
						<Form.Group id="password">
							<Form.Label>Mot de passe</Form.Label>
							<Form.Control type="password" ref={passwordRef} placeholder="Ne pas remplir pour garder le même mot de passe" />
						</Form.Group>
						<Form.Group id="passwordConfirm">
							<Form.Label>Confirmation du mot de passe</Form.Label>
							<Form.Control type="password" ref={passwordConfirmRef} placeholder="Ne pas remplir pour garder le même mot de passe" />
						</Form.Group>
						<Button disabled={loading} type="submit" className="w-100 mt-3">
							Mettre à jour
						</Button>
					</Form>
				</Card.Body>
			</Card>
			<div className="w100 text-center mt-2">
				<Link to="/">Annuler</Link>
			</div>
		</>
	);
}
