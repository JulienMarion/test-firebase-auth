import { Link } from 'react-router-dom';
import { useAuth } from '../../contexts/AuthContext';
import styles from './css/navigation.module.scss'

function Navigation() {
	const { currentUser } = useAuth();
	return (
		<header className={styles.header}>
			<Link className={styles.logo} to="/">FakeFlix</Link>
			<div>
				<nav>
					<ul>
						<li><Link to="/">Accueil</Link></li>
						{currentUser && <li><Link to="/profil">Mon Compte</Link></li>}
						{!currentUser && <li><Link to="/connexion">Connexion</Link></li>}
						{!currentUser && <li><Link to="/inscription">Inscription</Link></li>}
					</ul>
				</nav>
			</div>
		</header>
	);
}

export default Navigation;

// TODO: ajouter la page pour les details videos individuelles quand on click sur une video + player?
