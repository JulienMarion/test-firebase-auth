import Navigation from './Navigation';
import Footer from './Footer';
import styles from './css/layout.module.scss';

function Layout(props) {
	return (
		<>
			<Navigation />
			<main className={styles.main}>{props.children}</main>
			<Footer />
		</>
	);
}

export default Layout;
