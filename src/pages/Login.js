import { Container } from 'react-bootstrap';
import LoginForm from '../components/users/LoginForm';

function LoginPage() {
	return (
		<Container className="d-flex align-items-center justify-content-center">
			<div className="w-100" style={{ maxWidth: '500px' }}>
				<LoginForm />
			</div>
		</Container>
	);
}

export default LoginPage;
