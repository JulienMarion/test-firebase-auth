import { Container } from 'react-bootstrap';
import SignupForm from '../components/users/SignupForm';

function SubscribePage() {
	return (
		<Container className="d-flex align-items-center justify-content-center">
			<div className="w-100" style={{ maxWidth: '500px' }}>
				<SignupForm />
			</div>
		</Container>
	);
}

export default SubscribePage;
