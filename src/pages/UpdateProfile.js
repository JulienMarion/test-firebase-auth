import { Container } from 'react-bootstrap';
import UpdateProfileForm from '../components/users/UpdateProfileForm';

function LoginPage() {
	return (
		<Container className="d-flex align-items-center justify-content-center">
			<div className="w-100" style={{ maxWidth: '500px' }}>
				<UpdateProfileForm />
			</div>
		</Container>
	);
}

export default LoginPage;
