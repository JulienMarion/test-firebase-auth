import { Container } from 'react-bootstrap';
import ForgotPasswordForm from '../components/users/ForgotPasswordForm';

function LoginPage() {
	return (
		<Container className="d-flex align-items-center justify-content-center">
			<div className="w-100" style={{ maxWidth: '500px' }}>
				<ForgotPasswordForm />
			</div>
		</Container>
	);
}

export default LoginPage;
