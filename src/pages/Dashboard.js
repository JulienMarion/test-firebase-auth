import React, { useState } from 'react';
import { Card, Button, Alert, Container } from 'react-bootstrap';
import { useAuth } from '../contexts/AuthContext';
import { Link, useHistory } from 'react-router-dom';

export default function DashboardPage() {
	const [error, setError] = useState('');
	const { currentUser, logout } = useAuth();
	const history = useHistory();

	async function handleLogout() {
		setError('');
		try {
			await logout();
			history.pushState('/connexion');
		} catch {
			setError('Echec de la deconnexion');
		}
	}
	return (
		<Container className="d-flex align-items-center justify-content-center">
			<div className="w-100" style={{ maxWidth: '500px' }}>
				<Card>
					<Card.Body>
						<h2 className="text-center mb-4">Mon compte</h2>
						{error && <Alert variant="danger">{error}</Alert>}
						<strong>Email:</strong> {currentUser.email}
						<Link to="/modifierprofil" className="btn btn-primary w-100 mt-3">
							Modifier mon compte
						</Link>
					</Card.Body>
				</Card>
				<div className="w-100 text-center mt-2">
					<Button variant="link" onClick={handleLogout}>
						Se déconnecter
					</Button>
				</div>
			</div>
		</Container>
	);
}
